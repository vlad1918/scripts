package tools;

import java.io.File;

/**
 * Class that cleans all dirt before a SVN commit with MARA
 * @author vlad.dima
 *
 */
public class SvnCleaner {

	private String COMA_WEB 	 = "..\\CoMa\\coma.web";
	private String COMA_WEB_WAR  = COMA_WEB+"\\war\\coma.web"; 
	private String COMA_TOOLBAR  = COMA_WEB_WAR+"\\images\\tool-bar-button-icons"; 
	private String COMA_CSS 	 = COMA_WEB_WAR+"\\css"; 
	private String COMA_MANIFEST = COMA_WEB+"\\war\\WEB-INF\\deploy\\coma.web\\rpcPolicyManifest\\manifests";
	private String COMA_BASE	 = COMA_WEB_WAR+"\\base";
	private String COMA_BASE_IMG = COMA_WEB_WAR+"\\base\\images";
	private String COMA_BASE_BUT = COMA_WEB_WAR+"\\base\\images\\button";
	private String COMA_BASE_GRD = COMA_WEB_WAR+"\\base\\images\\grid";
	private String COMA_BLUE	 = COMA_WEB_WAR+"\\blue";
	private String COMA_BLUE_IMG = COMA_WEB_WAR+"\\blue\\images";
	private String COMA_BLUE_MNU = COMA_WEB_WAR+"\\blue\\images\\menu";
	private String COMA_BLUE_SLD = COMA_WEB_WAR+"\\blue\\images\\slider";
	
	private void clean() {
		
		int counter = 0;
		
		//Delete generated PDFs
		File comaWeb = new File(COMA_WEB);
		for (File file : comaWeb.listFiles()) {
			if (file.getName().startsWith("output-") && file.getName().endsWith(".pdf")){			
				file.delete();
				counter++;
			}
		}

		//Delete from war/coma.web
		File comaWebWar = new File(COMA_WEB_WAR);
		for (File file : comaWebWar.listFiles()) {
			if (file.getName().endsWith(".gwt.rpc") || file.getName().endsWith(".cache.png") 
				|| file.getName().equals("reset.css")){		
				file.delete();
				counter++;
			}
		}
		
		//Delete from images/tool-bar-button-icons
		File toolbar = new File(COMA_TOOLBAR);
		for (File file : toolbar.listFiles()) {
			if (! file.getName().equals("record-next.gif")){			
				file.delete();
				counter++;
			}
		}
		
		//Delete from css
		File css = new File(COMA_CSS);
		for (File file : css.listFiles()) {
			if (file.getName().equals("mmscommon.css")){			
				file.delete();
				counter++;
			}
		}
		
		//Delete from manifest
		File manif = new File(COMA_MANIFEST);
		for (File file : manif.listFiles()) {			
				file.delete();
				counter++;
		}
		
		//Delete from base image grid
		File grid = new File(COMA_BASE_GRD);
		for (File file : grid.listFiles()) {			
				file.delete();
				counter++;
		}
		
		//Delete from base image buttons
		File but = new File(COMA_BASE_BUT);
		for (File file : but.listFiles()) {			
				file.delete();
				counter++;
		}
		
		//Delete from base images
		File img = new File(COMA_BASE_IMG);
		for (File file : img.listFiles()) {			
				file.delete();
				counter++;
		}		
		
		//Delete from base
		File base = new File(COMA_BASE);
		for (File file : base.listFiles()) {			
			file.delete();
			counter++;
		}
		base.delete();

		//Delete from base image grid
		File sld = new File(COMA_BLUE_SLD);
		for (File file : sld.listFiles()) {			
				file.delete();
				counter++;
		}
		
		//Delete from base image buttons
		File menu = new File(COMA_BLUE_MNU);
		for (File file : menu.listFiles()) {			
				file.delete();
				counter++;
		}
		
		//Delete from base images
		File imgB = new File(COMA_BLUE_IMG);
		for (File file : imgB.listFiles()) {			
				file.delete();
				counter++;
		}		
		
		//Delete from base
		File blue = new File(COMA_BLUE);
		for (File file : blue.listFiles()) {			
			file.delete();
			counter++;
		}
		blue.delete();		
		
		System.out.println(counter+" files successfully removed");
	}
	
	public static void main(String[] args) {
		SvnCleaner sc = new SvnCleaner();
		sc.clean();
	}	
}
